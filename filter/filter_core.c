#include <linux/module.h>

int __init init_filter(void) {
	printk(KERN_INFO "filter load\n");
	return 0;
}

void __exit exit_filter(void) {
	printk(KERN_INFO "filter unload\n");
}

module_init(init_filter);
module_exit(exit_filter);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wonguk Lee");
MODULE_DESCRIPTION("filter driver for syscalls");
MODULE_VERSION("0.1");
