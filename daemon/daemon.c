#include <sys/queue.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

#define TASK_COMM_LEN		16
// pid of open event
struct event {
	pid_t pid;
	char comm[TASK_COMM_LEN];
	time_t timestamp;
	TAILQ_ENTRY(event) entries;
};

// product is one (single event queue)
TAILQ_HEAD(queue_head, event) g_event_queue;

// get/worker status
enum thread_status {
	INIT = 0,
	RUNNING,
	TERMINATING,
	TERMINATED,
};

#define MAX_GET_THREAD		3
#define MAX_WORKER_THREAD	3

struct thread_info {
	pthread_t tid;

	enum thread_status status;
	pthread_mutex_t mutex;		// status synchronize between main and pthread
};

struct thread_info g_get_thread_info[MAX_GET_THREAD];
struct thread_info g_worker_thread_info[MAX_WORKER_THREAD];

// no need to lock, since only main thread use this
int g_get_thread_cnt = 0;
int g_worker_thread_cnt = 0;

// queue
static void qinsert(struct event *ev) {
	TAILQ_INSERT_HEAD(&g_event_queue, ev, entries);
}

static struct event *qremove(void) {
	struct event *ev;

	ev = TAILQ_FIRST(&g_event_queue);
	if (ev)
		TAILQ_REMOVE(&g_event_queue, ev, entries);	

	// if queue empty, ev is NULL
	return ev;
}

static int check_get_cond(int tnum)
{
	int ret;
	enum thread_status status;

	pthread_mutex_lock(&g_get_thread_info[tnum].mutex);
	status = g_get_thread_info[tnum].status;
	if (status != RUNNING)
		ret = 1;		// terminate
	else
		ret = 0;
	pthread_mutex_unlock(&g_get_thread_info[tnum].mutex);

	return ret;
}

static char *get_current_comm(void)
{
	char *comm;

	FILE *cmdline = fopen("/proc/self/cmdline", "r");
	if (!cmdline)
		return NULL;

	comm = calloc(TASK_COMM_LEN, sizeof(char));
	if (!comm)
		return NULL;

	fscanf(cmdline, "%16s", comm);
	fclose(cmdline);

	return comm;
}

// get: get event from kernel, put event to event_queue
static void *get_routine(void *arg)
{
	struct event *ev;
	int tnum = *(int*)arg;
	char *comm;

	g_get_thread_info[tnum].status = RUNNING;

	while (!check_get_cond(tnum)) {
		// get event

		// put event
		ev = calloc(1, sizeof(struct event));
		if (!ev) {
			fprintf(stderr, "event calloc fail (%s(%d))\n", strerror(errno), errno);			
			pthread_exit(NULL);
			return NULL;		// no exit status code
		}

		ev->pid = getpid();
		comm = get_current_comm();
		if (comm) {
			strncpy(ev->comm, comm, TASK_COMM_LEN);
			ev->comm[TASK_COMM_LEN - 1] = 0;
			free(comm);
		}
		ev->timestamp = time(NULL);
		qinsert(ev);

		usleep(1000);	// 1ms
	}

	g_get_thread_info[tnum].status = TERMINATED;
}

static int check_worker_cond(int tnum)
{
	int ret;
	enum thread_status status;

	pthread_mutex_lock(&g_worker_thread_info[tnum].mutex);
	status = g_worker_thread_info[tnum].status;
	if (status != RUNNING)
		ret = 1;		// terminate
	else
		ret = 0;
	pthread_mutex_unlock(&g_worker_thread_info[tnum].mutex);

	return ret;
}

static void parse_event(struct event *ev)
{
	char timestr[20];
	strftime(timestr, sizeof(timestr), "%Y-%m-%d %H:%M:%S", localtime(&ev->timestamp));
	fprintf(stdout, "[%s] pid: %d, comm: %s\n", timestr, ev->pid, ev->comm);
}

// worker: get event from event_queue, print event
static void *worker_routine(void *arg)
{
	struct event *ev;
	int tnum = *(int*)arg;

	g_worker_thread_info[tnum].status = RUNNING;

	while (!check_worker_cond(tnum)) {
		ev = qremove();
		if (ev) {
			parse_event(ev);
			free(ev);
		}
		usleep(1000);		// 1ms
	}
	
	g_worker_thread_info[tnum].status = TERMINATED;
}

int spawn_get_thread(void)
{
	pthread_t tid;
	int ret;
	int tnum = g_get_thread_cnt;

	if (tnum >= MAX_GET_THREAD) {
		fprintf(stderr, "get thread maxinum number reached (tnum:%d)\n", tnum);
		return 1;	
	}

	ret = pthread_create(&tid, NULL, get_routine, &tnum);
	if (ret != 0) {
		fprintf(stderr, "pthread create fail (%s(%d): %d\n", strerror(errno), errno, tnum);
		return 1;
	}

	g_get_thread_info[tnum].tid = tid;
	g_get_thread_cnt++;
}

int spawn_worker_thread(void)
{
	pthread_t tid;
	int ret;
	int tnum = g_worker_thread_cnt;

	if (tnum >= MAX_WORKER_THREAD) {
		fprintf(stderr, "worker thread maxinum number reached (tnum:%d)\n", tnum);
		return 1;	
	}

	ret = pthread_create(&tid, NULL, worker_routine, &tnum);
	if (ret != 0) {
		fprintf(stderr, "pthread create fail (%s(%d): %d\n", strerror(errno), errno, tnum);
		return 1;
	}

	g_worker_thread_info[tnum].tid = tid;
	g_worker_thread_cnt++;
}

void destroy_get_thread(void)
{
	int tnum = g_get_thread_cnt - 1;

	pthread_mutex_lock(&g_get_thread_info[tnum].mutex);	
	g_get_thread_info[tnum].status = TERMINATING;
	pthread_mutex_unlock(&g_get_thread_info[tnum].mutex);	

	pthread_join(g_get_thread_info[tnum].tid, NULL);
	memset(&g_get_thread_info[tnum], 0, sizeof(struct thread_info));

	g_get_thread_cnt--;
}

void destroy_worker_thread(void)
{
	int tnum = g_worker_thread_cnt - 1;

	pthread_mutex_lock(&g_worker_thread_info[tnum].mutex);	
	g_worker_thread_info[tnum].status = TERMINATING;
	pthread_mutex_unlock(&g_worker_thread_info[tnum].mutex);	

	pthread_join(g_worker_thread_info[tnum].tid, NULL);
	memset(&g_worker_thread_info[tnum], 0, sizeof(struct thread_info));

	g_worker_thread_cnt--;
}

// daemon: init event_queue, spawn get/worker thread, control packet process
int main (int argc, char *argv[]) {
	struct event *ev;
	TAILQ_INIT(&g_event_queue);		

	spawn_get_thread();
	spawn_worker_thread();

	sleep(10);

	destroy_get_thread();
	destroy_worker_thread();

	return 0;
}
